#pragma once
#include <iostream>
#include <string>

using namespace std;

class Item
{

protected:
	int id;
	string name;

public:
	Item();
	Item(int newid, string desc);

	string display();

};
