#include <iostream>
#include <string>
#include <vector>
#include "Item.h"
#include "Inventory.h"
#include <sstream>
#include <stdio.h>
#include <functional>

using namespace std;

bool gameOver = false;
Inventory bag;
Inventory ground;
Item fish = Item(0, "fish");
Item cat = Item(0, "cat");
Item dog = Item(0, "dog");
Item mouse = Item(0, "mouse");

string getInput()
{
	string input;
	getline(cin, input);
	return input;
}

vector<string> split(string str, char delimiter) {
	vector<string> internal;
	stringstream ss(str); // Turn the string into a stream.
	string tok;

	while (getline(ss, tok, delimiter)) {
		internal.push_back(tok);
	}

	return internal;
}

void update(string input)
{
	if (input != " ")
	{
	
		vector<string> sep = split(input, ' ');

		if (sep.size() == 2)  //needs some check for incorrect user input and testing to see if item exsits
		{
			if (sep[0] == "pickup")
			{
				bag.addItem(ground.getItem(sep[1]));  
				ground.removeItem(sep[1]);
			}
			else if (sep[0] == "drop")
			{
				ground.addItem(bag.getItem(sep[1]));
				bag.removeItem(sep[1]);
			}
		}
	}	

	cout << "What is on the floor?" << endl;
	ground.display();
	cout << "------------------------------" << endl;
	cout << "What is in your bag?" << endl;
	bag.display();


	cout << "------------------------------" << endl;
}	



int main(int argc, const char* argv[])
{

	string input = " ";
	bag = Inventory(0, "bag");
	ground = Inventory(1, "ground");

	ground.addItem(fish);
	ground.addItem(cat);
	ground.addItem(dog);
	ground.addItem(mouse);

	cout << "What is this room, what is all the stuff on the floor?????" << endl;
	cout << "------------------------------" << endl;

	while (!gameOver)
	{

		update(input);
		input = getInput();
	}

	return 0;
}