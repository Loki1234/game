#include <iostream>
#include <string>
#include <stdio.h>
#include <ctype.h>

using namespace std;

char map[9][9] = {	{'#', '#', '#', '#', '#', '#', '#', '#'},
					{'#', 'G', ' ', 'D', '#', 'D', ' ', '#'},
					{'#', ' ', ' ', ' ', '#', ' ', ' ', '#'},
					{'#', '#', '#', ' ', '#', ' ', 'D', '#'},
					{'#', ' ', ' ', ' ', '#', ' ', ' ', '#'},
					{'#', ' ', '#', '#', '#', '#', ' ', '#'},
					{'#', ' ', ' ', ' ', ' ', ' ', ' ', '#'},
					{'#', '#', 'P', '#', '#', '#', '#', '#'},
					{'#', '#', '#', '#', '#', '#', '#', '#'}
																	};

int playeri;
int playerj;
bool gameOver = false;


char getInput()
{
	string opt;
	char move;

	for(int i = 0; i < 9; i++)
	{
		for(int j = 0; j < 9; j++)
		{
			if(map[i][j] == 'P')
			{
				playeri = i;
				playerj = j;
			}
		}
	}


	if(map[playeri][playerj + 1 ] != '#')
		opt += " E";

	if(map[playeri][playerj - 1] != '#')
		opt += " W";

	if(map[playeri - 1][playerj] != '#')
		opt += " N";

	if(map[playeri + 1][playerj] != '#')
		opt += " S";

	cout << "You can move" + opt + ":";
	cin >> move;

	return putchar(toupper(move));
}

void update(char m)
{
	if(m == 'N')
	{
		if(map[playeri - 1][playerj] == ' ')
		{
			map[playeri - 1][playerj] = 'P';
			map[playeri][playerj] = ' ';
		}

		if(map[playeri - 1][playerj] == 'G')
		{
			cout << "Wow you�ve discovered a large chest filled with GOLD coins! YOU WIN!";
			gameOver = true;
		}

		if(map[playeri - 1][playerj] == 'D')
		{
			cout << "Arrrrgh... you�ve fallen down a pit. YOU HAVE DIED! Thanks for playing. Maybe next time.";
			gameOver = true;
		}

		if(map[playeri - 1][playerj] == '#')
			cout << "Invalid Move";
	}

	if(m == 'E')
	{
		if(map[playeri][playerj + 1 ] == ' ')
		{
			map[playeri][playerj + 1 ] = 'P';
			map[playeri][playerj] = ' ';
		}

		if(map[playeri][playerj + 1 ] == 'G')
		{
			cout << "Wow you�ve discovered a large chest filled with GOLD coins! YOU WIN!";
			gameOver = true;
		}

		if(map[playeri][playerj + 1 ] == 'D')
		{
			cout << "Arrrrgh... you�ve fallen down a pit. YOU HAVE DIED! Thanks for playing. Maybe next time.";
			gameOver = true;
		}

		if(map[playeri][playerj + 1 ] == '#')
			cout << "Invalid Move";
	}

	if(m == 'W')
	{
		if(map[playeri][playerj - 1] == ' ')
		{
			map[playeri][playerj - 1] = 'P';
			map[playeri][playerj] = ' ';
		}

		if(map[playeri][playerj - 1] == 'G')
		{
			cout << "Wow you�ve discovered a large chest filled with GOLD coins! YOU WIN!";
			gameOver = true;
		}

		if(map[playeri][playerj - 1] == 'D')
		{
			cout << "Arrrrgh... you�ve fallen down a pit. YOU HAVE DIED! Thanks for playing. Maybe next time.";
			gameOver = true;
		}

		if(map[playeri][playerj - 1] == '#')
			cout << "Invalid Move";
	}

	
	if(m == 'S')
	{
		if(map[playeri + 1][playerj] == ' ')
		{
			map[playeri + 1][playerj] = 'P';
			map[playeri][playerj] = ' ';
		}

		if(map[playeri + 1][playerj] == 'G')
		{
			cout << "Wow you have discovered a large chest filled with GOLD coins! YOU WIN!";
			gameOver = true;
		}

		if(map[playeri + 1][playerj] == 'D')
		{
			cout << "Arrrrgh... you have fallen down a pit. YOU HAVE DIED! Thanks for playing. Maybe next time.";
			gameOver = true;
		}

		if(map[playeri + 1][playerj] == '#')
			cout << "Invalid Move" << endl;
	}
}

void display()
{
	system("CLS");
	for(int i=0; i<9; i++)    
	{
		for(int j=0; j<9; j++) 
		{
			cout << map[i][j]  << "  ";
		}
		cout << endl;
	}
}

int main ()
{

	char move;

	cout << "Welcome to GridWorld: Quantised Excitement. Fate is waiting for You! Valid commands: N, S, E and W for direction. Q to quit the game." << endl;
	display();

	while(!gameOver)
	{
		
		move = getInput();

		update(move);

		if(!gameOver)
			display();

	}
	cout << endl;
	system("pause");
	return 0;
}