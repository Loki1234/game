// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectBase.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPIKE17_MyActor_generated_h
#error "MyActor.generated.h already included, missing '#pragma once' in MyActor.h"
#endif
#define SPIKE17_MyActor_generated_h

#define Spike17_Source_Spike17_Public_MyActor_h_11_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execgetVector) \
	{ \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_outX); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_outY); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_outZ); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->getVector(Z_Param_Out_outX,Z_Param_Out_outY,Z_Param_Out_outZ); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetVector) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_newX); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_newY); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_newZ); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->setVector(Z_Param_newX,Z_Param_newY,Z_Param_newZ); \
		P_NATIVE_END; \
	}


#define Spike17_Source_Spike17_Public_MyActor_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execgetVector) \
	{ \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_outX); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_outY); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_outZ); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->getVector(Z_Param_Out_outX,Z_Param_Out_outY,Z_Param_Out_outZ); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetVector) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_newX); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_newY); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_newZ); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->setVector(Z_Param_newX,Z_Param_newY,Z_Param_newZ); \
		P_NATIVE_END; \
	}


#define Spike17_Source_Spike17_Public_MyActor_h_11_INCLASS_NO_PURE_DECLS \
	private: \
	static void StaticRegisterNativesAMyActor(); \
	friend SPIKE17_API class UClass* Z_Construct_UClass_AMyActor(); \
	public: \
	DECLARE_CLASS(AMyActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Spike17"), NO_API) \
	DECLARE_SERIALIZER(AMyActor) \
	/** Indicates whether the class is compiled into the engine */ \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Spike17_Source_Spike17_Public_MyActor_h_11_INCLASS \
	private: \
	static void StaticRegisterNativesAMyActor(); \
	friend SPIKE17_API class UClass* Z_Construct_UClass_AMyActor(); \
	public: \
	DECLARE_CLASS(AMyActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Spike17"), NO_API) \
	DECLARE_SERIALIZER(AMyActor) \
	/** Indicates whether the class is compiled into the engine */ \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Spike17_Source_Spike17_Public_MyActor_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyActor(AMyActor&&); \
	NO_API AMyActor(const AMyActor&); \
public:


#define Spike17_Source_Spike17_Public_MyActor_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyActor(AMyActor&&); \
	NO_API AMyActor(const AMyActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyActor)


#define Spike17_Source_Spike17_Public_MyActor_h_8_PROLOG
#define Spike17_Source_Spike17_Public_MyActor_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Spike17_Source_Spike17_Public_MyActor_h_11_RPC_WRAPPERS \
	Spike17_Source_Spike17_Public_MyActor_h_11_INCLASS \
	Spike17_Source_Spike17_Public_MyActor_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Spike17_Source_Spike17_Public_MyActor_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Spike17_Source_Spike17_Public_MyActor_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	Spike17_Source_Spike17_Public_MyActor_h_11_INCLASS_NO_PURE_DECLS \
	Spike17_Source_Spike17_Public_MyActor_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Spike17_Source_Spike17_Public_MyActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
