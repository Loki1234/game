// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectBase.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPIKE17_VectorTest_generated_h
#error "VectorTest.generated.h already included, missing '#pragma once' in VectorTest.h"
#endif
#define SPIKE17_VectorTest_generated_h

#define Spike17_Source_Spike17_Public_VectorTest_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execsetVector) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_newX); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_newY); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_newZ); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->setVector(Z_Param_newX,Z_Param_newY,Z_Param_newZ); \
		P_NATIVE_END; \
	}


#define Spike17_Source_Spike17_Public_VectorTest_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execsetVector) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_newX); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_newY); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_newZ); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->setVector(Z_Param_newX,Z_Param_newY,Z_Param_newZ); \
		P_NATIVE_END; \
	}


#define Spike17_Source_Spike17_Public_VectorTest_h_12_INCLASS_NO_PURE_DECLS \
	private: \
	static void StaticRegisterNativesUVectorTest(); \
	friend SPIKE17_API class UClass* Z_Construct_UClass_UVectorTest(); \
	public: \
	DECLARE_CLASS(UVectorTest, UActorComponent, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Spike17"), NO_API) \
	DECLARE_SERIALIZER(UVectorTest) \
	/** Indicates whether the class is compiled into the engine */ \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Spike17_Source_Spike17_Public_VectorTest_h_12_INCLASS \
	private: \
	static void StaticRegisterNativesUVectorTest(); \
	friend SPIKE17_API class UClass* Z_Construct_UClass_UVectorTest(); \
	public: \
	DECLARE_CLASS(UVectorTest, UActorComponent, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Spike17"), NO_API) \
	DECLARE_SERIALIZER(UVectorTest) \
	/** Indicates whether the class is compiled into the engine */ \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Spike17_Source_Spike17_Public_VectorTest_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVectorTest(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVectorTest) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVectorTest); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVectorTest); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVectorTest(UVectorTest&&); \
	NO_API UVectorTest(const UVectorTest&); \
public:


#define Spike17_Source_Spike17_Public_VectorTest_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVectorTest(UVectorTest&&); \
	NO_API UVectorTest(const UVectorTest&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVectorTest); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVectorTest); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UVectorTest)


#define Spike17_Source_Spike17_Public_VectorTest_h_9_PROLOG
#define Spike17_Source_Spike17_Public_VectorTest_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Spike17_Source_Spike17_Public_VectorTest_h_12_RPC_WRAPPERS \
	Spike17_Source_Spike17_Public_VectorTest_h_12_INCLASS \
	Spike17_Source_Spike17_Public_VectorTest_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Spike17_Source_Spike17_Public_VectorTest_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Spike17_Source_Spike17_Public_VectorTest_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Spike17_Source_Spike17_Public_VectorTest_h_12_INCLASS_NO_PURE_DECLS \
	Spike17_Source_Spike17_Public_VectorTest_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Spike17_Source_Spike17_Public_VectorTest_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
