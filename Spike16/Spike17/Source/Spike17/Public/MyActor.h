// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MyActor.generated.h"

UCLASS()
class SPIKE17_API AMyActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	float X = 0.0f;
	float Y = 0.0f;
	float Z = 0.0f;

	UFUNCTION(BlueprintCallable, Category = "TESTING")
		void setVector(float newX, float newY, float newZ);

	UFUNCTION(BlueprintCallable, Category = "TESTING")
		void getVector(float &outX, float &outY, float &outZ);
	
};
