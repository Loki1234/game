#include "GameplayState.h"
#include <functional>
#include <sstream>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <fstream>

using namespace std;

GameplayState::GameplayState()
{

}

GameplayState::GameplayState(string fileName)
{
	vector<string> lines;
	string line;
	ifstream myfile(fileName);
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			lines.push_back(line);
		}
		myfile.close();
	}

	for (int i = lines.size() - 1; i >= 0; i--) {
		cout << lines[i] << endl;
	}
}

void GameplayState::display()
{

}

string GameplayState::getInput()
{
	string input;
	getline(cin, input);
	return input;
}

void GameplayState::update(string input)
{

}

vector<string> GameplayState::split(string str, char delimiter) {
	vector<string> internal;
	stringstream ss(str); // Turn the string into a stream.
	string tok;

	while (getline(ss, tok, delimiter)) {
		internal.push_back(tok);
	}

	return internal;
}

