
#include <iostream>
#include <string>
#include <functional>
#include <vector>

#ifndef GAMEPLAYSTATE_H
#define GAMEPLAYSTATE_H

using namespace std;

class GameplayState
{
protected:
	string desc;

public:
	GameplayState();
	GameplayState(string fileName);
	void update(string input);
	string getInput();
	void display();
	vector<string> split(string str, char delimiter);

};
#endif 