#include "StateManager.h"
#include "gameState.h"
#include "GameplayState.h"
using namespace std;

StateManager::StateManager()
{
	gameOver = false;
	currentState = 0;
	
	states[0] = gameState(0, 
		"Zorkish :: Main Menu \n ------------------------------------------------------- \n "
		"\n Welcome to Zorkish"
		"\n \n 1.Select Adventure and Play"
		"\n 2.Hall of Fame"
		"\n 3.Help"
		"\n 4.About"
		"\n 5.Quit"
		"\n \n Select 1-5 :>"
);
	states[1] = gameState(1,
		"Zorkish :: Zorkish || Select Adventure \n ------------------------------------------------------- \n "
		"\n Choose your adventure \n"
		"\n 1.Test world"
		);

	states[2] = gameState(2,
		"Zorkish :: Zorkish || Hall of Fame \n ------------------------------------------------------- \n "
		"\n Top 10 Zorkish Adventure Champions \n"
		"\n 1. Lewis Herrald , Test World, 6969"
		"\n 2. Lewis Herrald , Test World, 6969"
		"\n 3. Lewis Herrald , Test World, 6969"
		"\n Press 5 to return to the Main Menu"
		);
	states[3] = gameState(3,
		"Zorkish :: Zorkish || Help \n ------------------------------------------------------- \n "
		"\n Quit, \n"
		"\n hiscore"
		"\n Press 5 to return to the Main Menu"
		);

	states[4] = gameState(4,
		"Zorkish :: Zorkish || About \n ------------------------------------------------------- \n "
		"\n Written by: Lewis Herrald"
		"\n Press 5 to return to the Main Menu"
		);
	

	game = GameplayState("test.txt");
}

void StateManager::display()
{
	switch (currentState) {
	case 0:
		states[0].display();
		break;
	case 1:
		states[1].display();
		break;
	case 2:
		states[2].display();
		break;
	case 3:
		game.display();
		break;
	case 4:
		states[4].display();
		break;
	}
}

string StateManager::input()
{
	switch (currentState) {
	case 0:
		return states[0].getInput();
		break;
	case 1:
		return states[1].getInput();
		break;
	case 2:
		return states[2].getInput();
		break;
	case 3:
		return game.getInput();
		break;
	case 4:
		return states[4].getInput();
		break;
	}

	return 0;
}

void StateManager::update(string input)
{
	switch (currentState) {
	case 0:
		if (input == "1")
			currentState = 1;
		if (input == "2")
			currentState = 2;
		if (input == "3")
			currentState = 3;
		if (input == "4")
			currentState = 4;
		if (input == "5")
			gameOver = true;
		break;
	case 1:
		if (input == "1")
			currentState = 3;
		break;
	case 2:
		if (input == "5")
			currentState = 0;
		break;
	case 3:
		game.update(input);
		break;
	case 4:
		if (input == "5")
			currentState = 0;
		break;
	}

}

bool StateManager::GameOver()
{
	return gameOver;
}