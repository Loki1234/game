#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Item.h"

#ifndef INVENTORY_H
#define INVENTORY_H

using namespace std;

class Inventory
{

protected:
	int id;
	//string name;
	int size;
	//vector<Item> Items;

public:
	vector<Item> Items;
	string name;
	Inventory();
	Inventory(int newid, string desc);
	Inventory(int newid, string desc, int s);

	void display();
	void addItem(Item i);
	void removeItem(string ite);
	Item getItem(string ite);

};
#endif 