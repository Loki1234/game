#include <iostream>
#include <string>
#include <vector>
#include "Item.h"
#include "Inventory.h"

using namespace std;

Inventory::Inventory()
{

}

Inventory::Inventory(int newid, string newdesc)
{
	id = newid;
	name = newdesc;
}

Inventory::Inventory(int newid, string newdesc, int s)
{
	id = newid;
	name = newdesc;
	size = s;
	Items.resize(s);
}

void Inventory::display()
{
	for (auto & element : Items) {
		cout << element.display() << endl;
	}

}
void Inventory::addItem(Item i)
{
	Items.push_back(i);
}

void Inventory::removeItem(string ite)
{
	for (int i = Items.size() - 1; i >= 0; i--) {
		if (Items[i].display() == ite)
		{
			Items.erase(Items.begin() + i);
		}
	}
}


Item Inventory::getItem(string ite)
{
	for (int i = Items.size() - 1; i >= 0; i--) {
		if (Items[i].name == ite)
		{
			return Items[i];
		}
	}

	Item dud = Item();
	return dud;
}