#pragma once
#include <iostream>
#include <string>
#include "GameplayState.h"
#include "GoCommand.h"
#include "LookCommand.h"
#include "OpenCommand.h"

#ifndef COMMANDMANAGER_H
#define COMMANDMANAGER_H

using namespace std;

class CommandManager
{
private:
	GoCommand Go;
	//LookCommand look;
public:
	CommandManager();
	void runCommand(string rawInput,string firstInput, GameplayState& world);

};
#endif 