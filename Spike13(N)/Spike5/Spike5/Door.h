#pragma once

#include <iostream>
#include <string>

#ifndef DOOR_H
#define DOOR_H

using namespace std;

class Door
{

protected:
	int leadingRoom;
	

public:
	bool locked;

	string desc;
	Door();
	Door(string Newdesc,int leadRoom);
	Door(string Newdesc,int leadRoom, string lock );
	void unlockDoor();
	int getLeadingRoom();

};
#endif 