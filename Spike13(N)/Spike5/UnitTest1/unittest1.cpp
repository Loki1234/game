#include "stdafx.h"
#include "CppUnitTest.h"
#include "C:\Users\Lewis\Desktop\GameFinal\Spike13(N)\Spike5\Spike5\Inventory.h"
#include "C:\Users\Lewis\Desktop\GameFinal\Spike13(N)\Spike5\Spike5\Inventory.cpp"

#include "C:\Users\Lewis\Desktop\GameFinal\Spike13(N)\Spike5\Spike5\Item.h"
#include "C:\Users\Lewis\Desktop\GameFinal\Spike13(N)\Spike5\Spike5\Item.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(AddItemTest)
		{
			//playerInventory.addItem(Item(0, "key for door", "Key"));

			Inventory i = Inventory(1, "test");
			Item item = Item(0, "Key", "key to door");

			i.addItem(item);
			Item item2 = i.getItem("Key");
			int size = i.Items.size();

			Assert::AreEqual(item.name,  item2.name);
			Assert::AreEqual(item.desc, item2.desc);
			Assert::AreEqual(1, size);
		

			// TODO: Your test code here
		}


		TEST_METHOD(SizeTest)
		{
			//playerInventory.addItem(Item(0, "key for door", "Key"));

			Inventory i = Inventory(1, "test", 2);
			Item item = Item(0, "Key", "key to door");
			int size = i.Items.size();
			

			Assert::AreEqual(2, size);
		


			// TODO: Your test code here
		}

		TEST_METHOD(DudItemTest)
		{
			//playerInventory.addItem(Item(0, "key for door", "Key"));

			Inventory i = Inventory(1, "test");
			Item item = Item(0, "Key", "key to door");

			i.addItem(item);
			Item item2 = i.getItem("test"); //should return dud item

			string test = item2.name;
			string dud = "dud";

			Assert::AreNotEqual(item.name, item2.name);
			Assert::AreEqual(dud, test);


			// TODO: Your test code here
		}

		TEST_METHOD(RemoveTest)
		{
			//playerInventory.addItem(Item(0, "key for door", "Key"));

			Inventory i = Inventory(1, "test");
			Item item = Item(0, "Key", "key to door");
			i.removeItem("Key");
			int size = i.Items.size();


			Assert::AreEqual(0, size);



			// TODO: Your test code here
		}

	};
}