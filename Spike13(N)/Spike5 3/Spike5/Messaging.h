#pragma once
#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

#ifndef MESSAGING_H
#define MESSAGING_H

using namespace std;

class Messaging
{
private:
	vector<string> boxMessage;
public:	
	Messaging();
	void addMessage(string adrss, string message);
	vector<string> getMessages(string adrss);
	void clearMessage(string adrss);

};
#endif 