#pragma once
#include <iostream>
#include <string>
#include "GameplayState.h"
#ifndef TAKEOMMAND_H
#define TAKECOMMAND_H

using namespace std;

class TakeCommand
{
public:
	void run(string input, GameplayState& world);
};
#endif 