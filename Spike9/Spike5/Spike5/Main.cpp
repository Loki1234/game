#include <stdio.h>
#include <iostream>
#include "stateManager.h"


using namespace std;

int main( int argc, const char* argv[] )
{
	
	StateManager game;
	string input;
	while (!game.GameOver())
	{
		game.display();
		input = game.input();
		game.update(input);
	}


	//create a gameplay object that is the game, seperate from game State
	return 0;
}