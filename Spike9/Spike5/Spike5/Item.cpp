#include "Item.h"
#include <iostream>
#include <string>

using namespace std;

Item::Item() 
{
	name = "dud";
}

Item::Item(int newid, string ItName, string descp)
{
	id = newid;
	name = ItName;
	desc = descp;
	pickup = false;
}

Item::Item(int newid, string ItName, string descp, bool p)
{
	id = newid;
	name = ItName;
	desc = descp;
	pickup = p;
}

void Item::setPickup(bool p)
{
	pickup = p;
}

string Item::display()
{
	return name;
}