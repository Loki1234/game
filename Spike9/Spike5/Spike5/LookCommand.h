#pragma once
#include <iostream>
#include <string>
#include "GameplayState.h"
#ifndef LOOKCOMMAND_H
#define LOOKCOMMAND_H

using namespace std;

class LookCommand
{
public:
	void run(string input, GameplayState& world);
};
#endif
