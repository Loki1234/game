#include "LookCommand.h"
#include <iostream>
#include <string>
#include "Item.h"
#include "Room.h"

using namespace std;

void LookCommand::run(string input, GameplayState& world)
{
	vector<string> comand;
	comand = world.split(input, ' ');
	bool invenFound = false;

	if (comand.size() > 2)
	{
		if (comand[1] == "at")
		{
			Room dec = world.rooms[world.currentRoom];
			Item test = dec.Inven.getItem(comand[2]);

			if (test.name != "dud")
				cout << test.display() << endl;
			else
				cout << "Item not found" << endl;
		}
		else if (comand[1] == "in")
		{
			Room dec = world.rooms[world.currentRoom];
			Item test = dec.Inven.getItem(comand[2]);
			if (test.name != "dud")
				if (world.inev.size() > 0)
				{
					//cout << test.name + "Contains:" << endl;
					for (int k = 0; k < world.inev.size(); k++)
					{
						if (world.inev[k].name == test.name)
						{
							cout << test.name + " Contains: " << endl;
							world.inev[k].display();
							invenFound = true;
						}
					}
					if(!invenFound)
						cout << test.name + " is empty "<< endl;
				}
			else
				cout << "Item not found" << endl;
		}
	}
	else
		cout << "Look Error" << endl;
	
}