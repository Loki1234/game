
#include <iostream>
#include <string>
#include <functional>
#include <vector>
#include "Room.h"
#include "CommandManager.h"
#include "Inventory.h"

#ifndef GAMEPLAYSTATE_H
#define GAMEPLAYSTATE_H

using namespace std;

class GameplayState
{
public:
	string desc;
	vector<Room> rooms;
	vector<Inventory> inev;
	Inventory playerInventory;
	int currentRoom;
	GameplayState();
	GameplayState(string fileName);
	void update(string input);
	string getInput();
	void display();
	vector<string> split(string str, char delimiter);

};
#endif 