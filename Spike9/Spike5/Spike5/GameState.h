
#include <iostream>
#include <string>

#ifndef GAMESTATE_H
#define GAMESTATE_H

using namespace std;

class gameState
{

protected:
	int id;
	char input;
	string desc;

public:
	gameState();
	gameState(int newid, string desc);

	void update();
	string getInput();
	void display();

	int getId();
	void setId(int id);

};
#endif 