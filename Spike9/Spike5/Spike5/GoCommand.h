#pragma once
#include <iostream>
#include <string>
#include "GameplayState.h"
#ifndef GOCOMMAND_H
#define GOCOMMAND_H

using namespace std;

class GoCommand
{
public:
	void run(string input, GameplayState& world);
};
#endif 