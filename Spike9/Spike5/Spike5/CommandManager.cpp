#include "CommandManager.h"
#include <iostream>
#include <string>
#include "LookCommand.h"
#include "OpenCommand.h"
#include "takeCommand.h"
using namespace std;


CommandManager::CommandManager()
{
	
}

void CommandManager::runCommand(string rawInput,string firstInput, GameplayState& world)
{
	LookCommand look;
	OpenCommand open;
	TakeCommand take;
	//case statment here for each command 
	if(firstInput == "Go" || firstInput == "go" || firstInput == "gO")
		Go.run(rawInput, world); 
	if (firstInput == "Look" || firstInput == "look" )
		look.run(rawInput, world); 
	if (firstInput == "Open" || firstInput == "open")
		open.run(rawInput, world);
	if (firstInput == "Take" || firstInput == "take")
		take.run(rawInput, world);
}