#pragma once

#include <iostream>
#include "canBeAttacked.h"
#include "damage.h"
#include "health.h"

#ifndef PLAYER_H
#define PLAYER_H

using namespace std;

class Player
{

public:
	Health playerHealth;
	Damage playerDamage;
	CanBeAttacked playerAttacked;
	Player();


};
#endif 


