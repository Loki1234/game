#include <iostream>
#include <string>
#include "gameState.h"
#include "GameplayState.h"

using namespace std;

class StateManager
{
private:
	int currentState;
	bool gameOver;
	gameState menu;
	gameState states[5];
	GameplayState game;

public:

	StateManager();
	string input();
	void display();
	void update(string input);

	bool GameOver();

};