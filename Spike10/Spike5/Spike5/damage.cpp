#include "damage.h"
#include <stdio.h>
#include <iostream>

using namespace std;

Damage::Damage(int h, bool attack)
{
	damageInt = h;
	canAttackBool= attack;
}

int Damage::getDamage() { return damageInt; }
bool Damage::canAttack() { return canAttackBool; }

