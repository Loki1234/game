#include "gameState.h"

using namespace std;

gameState::gameState()
{

}

gameState::gameState(int newid, string newdesc)
{
	id = newid;
	desc = newdesc;
}

void gameState::display()
{
	cout << desc << endl;
}

string gameState::getInput()
{
	string input;
	cin >> input;
	system("CLS");
	return input;
}

void gameState::update()
{

}

int gameState::getId()
{
	return id;
}

void gameState::setId(int i)
{
	id = i;
}
