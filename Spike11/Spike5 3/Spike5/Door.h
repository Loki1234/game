#pragma once

#include <iostream>
#include <string>

#ifndef DOOR_H
#define DOOR_H

using namespace std;

class Door
{

protected:
	int leadingRoom;
	bool locked;

public:
	string desc;
	Door();
	Door(string Newdesc,int leadRoom);
	Door(string Newdesc,int leadRoom, bool lock );
	int getLeadingRoom();

};
#endif 