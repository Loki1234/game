#pragma once

#include <iostream>

#ifndef CANBEATTACKED_H
#define CANBEATTACKED_H

using namespace std;

class CanBeAttacked
{

private:
	bool attackBool;

public:
	CanBeAttacked(bool h);
	bool canBeAttack();

};
#endif 


