#include "GameplayState.h"
#include <functional>
#include <sstream>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <fstream>
#include "Room.h"

using namespace std;

GameplayState::GameplayState()
{

}

GameplayState::GameplayState(string fileName)
{
	int doorJump = 0;
	vector<string> lines;
	vector<string> roomPieces;
	vector<Door> Doors;
	string line;
	ifstream myfile(fileName);
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			lines.push_back(line);
		}
		myfile.close();
	}

	for (int i = lines.size() - 1; i >= 0; i--) {
		roomPieces = split(lines[i], ';');
		doorJump = 0;
		for (int a = 0; a < atoi(roomPieces[1].c_str()); a++)
		{
			Doors.push_back(Door(roomPieces[2 + doorJump], atoi(roomPieces[3 + doorJump].c_str())));
			doorJump = doorJump + 3;
		}

		rooms.push_back(Room(roomPieces[0], Doors));	
	}

	currentRoom = 0;

	//for (int i = lines.size() - 1; i >= 0; i--) {
		//cout << lines[i] << endl;
	//}
}

void GameplayState::display()
{
	rooms[currentRoom].display();
}

string GameplayState::getInput()
{
	string input;
	getline(cin, input);
	return input;
}

void GameplayState::update(string input)
{
	
	vector<string> comand;
	int newLoc;
	comand = split(input, ' ');
	if  (comand.size() == 2)
	{
		if (comand[0] == "go" || comand[0] == "Go" || comand[0] == "GO")
		{
			newLoc = rooms[currentRoom].findDoorLoc(comand[1]);
			if (newLoc != 1000)
				currentRoom = newLoc;
		}
		//-----------------------------Component SHOW OFF HERE !!!!!!!!!!
	
		player.update(messMan,comand);
		shootBox.update(messMan);
		
	}
}

vector<string> GameplayState::split(string str, char delimiter) {
	vector<string> internal;
	stringstream ss(str); // Turn the string into a stream.
	string tok;

	while (getline(ss, tok, delimiter)) {
		internal.push_back(tok);
	}

	return internal;
}

