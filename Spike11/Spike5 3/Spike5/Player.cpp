#include "Player.h"
#include <stdio.h>
#include <iostream>
#include <sstream>

using namespace std;

Player::Player():playerHealth(10),playerDamage(4,true), playerAttacked(true)
{
	
}

void Player::update(Messaging& m, vector<string> input)
{
	if (input[0] == "Shoot" || input[0] == "shoot")
	{
		if (playerDamage.canAttack())
		{
			string shot = "s ";
			string dam = static_cast<ostringstream*>(&(ostringstream() << playerDamage.getDamage()))->str();

			shot.append(dam);
			m.addMessage(input[1], shot);
		}
		else
			cout << "Player can not attack" << endl;
	}
	if (input[0] == "Heal" || input[0] == "heal")
	{			
		if (input[0] != "self")
			playerHealth.addhealth(4);
		else
		{
			string heal = "h 4";
			m.addMessage(input[1], heal);
		}	
	}

	string dam = static_cast<ostringstream*>(&(ostringstream() << playerHealth.getHelth()))->str();
	cout << "You have " + dam + " health" << endl;
}

