#pragma once

#include <iostream>
#include "canBeAttacked.h"
#include "damage.h"
#include "Messaging.h"
#include "health.h"

#ifndef BOX_H
#define BOX_H

using namespace std;

class Box
{

public:
	Health boxHealth;
	Damage boxDamage;
	CanBeAttacked boxAttacked;
	Box();
	void update(Messaging& m);
	vector<string> split(string str, char delimiter);

};
#endif 
