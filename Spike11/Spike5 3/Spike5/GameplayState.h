
#include <iostream>
#include <string>
#include <functional>
#include <vector>
#include "Room.h"
#include "Box.h"
#include "Player.h"
#include "Messaging.h"

#ifndef GAMEPLAYSTATE_H
#define GAMEPLAYSTATE_H

using namespace std;

class GameplayState
{
protected:
	string desc;
	vector<Room> rooms;
	int currentRoom;

	// component classes
	Player player;
	Box shootBox;
	
public:
	Messaging messMan;
	GameplayState();
	GameplayState(string fileName);
	void update(string input);
	string getInput();
	void display();
	vector<string> split(string str, char delimiter);

};
#endif 