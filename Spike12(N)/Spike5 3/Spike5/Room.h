#pragma once

#include <iostream>
#include <string>
#include "Door.h"
#include <vector>
#include "Messaging.h"

#ifndef ROOM_H
#define ROOM_H

using namespace std;

class Room
{

protected:
	string desc;
	vector<Door> doors;

public:
	Room();
	Room(string Doordesc, vector<Door> newDoors);
	void display();
	int findDoorLoc(string desc);
	void update(Messaging& m);

};
#endif 