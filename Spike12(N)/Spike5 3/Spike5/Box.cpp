#include "Box.h"
#include <stdio.h>
#include <iostream>
#include <sstream>


using namespace std;

Box::Box() :boxHealth(10), boxDamage(0, false), boxAttacked(true)
{

}

void Box::update(Messaging& m)
{
	vector<string> mess = m.getMessages("box");
	int i = 0;
	string messstring;

	if (mess.size() > 0)
	{
		vector<string>::iterator it;
		for (it = mess.begin(); it < mess.end(); it++, i++) {
			messstring = *it;
			vector<string> message = split(messstring,' ');

			if (message[0] == "s")
			{
				if (!boxHealth.dead())
				{
					if (boxAttacked.canBeAttack())
					{				
						boxHealth.removeHealth(atoi(message[1].c_str()));
						cout << "the box took " + message[1] + " damage" << endl;
					}
					else
						cout << "box cant be attacked" << endl;
				}
				else
					cout << "box is dead" << endl;
			}
			if (message[0] == "h")
			{
				if (!boxHealth.dead())
				{			
					boxHealth.addhealth(atoi(message[1].c_str()));
					cout << "the was healed " + message[1] << endl;				
				}
				else
					cout << "box is dead" << endl;
			}
		}
	}

	m.clearMessage("box");
	string dam = static_cast<ostringstream*>(&(ostringstream() << boxHealth.getHelth()))->str();
	cout << "the box has " + dam + " health" << endl;

}

vector<string> Box::split(string str, char delimiter) {
	vector<string> internal;
	stringstream ss(str); // Turn the string into a stream.
	string tok;

	while (getline(ss, tok, delimiter)) {
		internal.push_back(tok);
	}

	return internal;
}



