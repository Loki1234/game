#pragma once

#include <iostream>

#ifndef HEALTH_H
#define HEALTH_H

using namespace std;

class Health
{

private:
	int health;

public:
	Health(int h);
	void removeHealth(int d);
	void addhealth(int h);
	bool dead();
	int getHelth();
};
#endif 
