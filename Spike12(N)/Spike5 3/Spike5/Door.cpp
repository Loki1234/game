#include "Door.h"
#include <functional>
#include <sstream>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <fstream>

using namespace std;

Door::Door()
{

}

Door::Door(string Newdesc, int leadRoom)
{
	desc = Newdesc;
	leadingRoom = leadRoom;
	locked = false;
}

Door::Door(string Newdesc, int leadRoom, bool lock)
{
	desc = Newdesc;
	leadingRoom = leadRoom;
	locked = lock;
}

int Door::getLeadingRoom(){return leadingRoom;}

void Door::readMessage(string m)
{
	if (m == "lock")
		locked = true;

	if (m == "open")
		locked = false;

}

