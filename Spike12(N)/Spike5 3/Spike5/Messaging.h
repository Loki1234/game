#pragma once
#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

#ifndef MESSAGING_H
#define MESSAGING_H

using namespace std;

class Messaging
{
private:
	vector<string> boxMessage;
	string DoorAnnouc = "blank";

public:	
	Messaging();
	void addMessage(string adrss, string message);
	void addMessageA(string message,string a);
	vector<string> getMessages(string adrss);
	void clearMessage(string adrss);
	string readA(string addr);

};
#endif 