#include "Room.h"
#include <functional>
#include <sstream>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <fstream>

using namespace std;

Room::Room()
{

}

Room::Room(string Doordesc, vector<Door> newDoors)
{
	desc = Doordesc;
	doors = newDoors;
}

void Room::display()
{
	cout << desc << endl;
}

int Room::findDoorLoc(string desc)
{
	for (int i = doors.size() - 1; i >= 0; i--) 
		{
			if (doors[i].desc == desc && !doors[i].locked)
				return doors[i].getLeadingRoom();
		}

		return 1000;
}

void Room::update(Messaging& m)
{
	for (int i = doors.size() - 1; i >= 0; i--)
	{		
		doors[i].readMessage(m.readA("doors"));
	}

}





