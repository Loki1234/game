#pragma once

#include <iostream>

#ifndef DAMAGE_H
#define DAMAGE_H

using namespace std;

class Damage
{

private:
	int damageInt;
	bool canAttackBool;

public:
	Damage(int h, bool attack);
	int getDamage();
	bool canAttack();
	
};
#endif 

