#pragma once

#include <iostream>
#include <string>
#include "Door.h"
#include <vector>
#include "Inventory.h"

#ifndef ROOM_H
#define ROOM_H

using namespace std;

class Room
{

protected:
	string desc;
	//vector<Door> doors;
	

public:
	vector<Door> doors;
	Inventory Inven;
	Room();
	Room(string Doordesc, vector<Door> newDoors);
	void display();
	int findDoorLoc(string desc);

};
#endif 