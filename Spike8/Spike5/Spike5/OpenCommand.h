#pragma once
#include <iostream>
#include <string>
#include "GameplayState.h"
#ifndef OPENCOMMAND_H
#define OPENCOMMAND_H

using namespace std;

class OpenCommand
{
public:
	void run(string input, GameplayState& world);
};
#endif 