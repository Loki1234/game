#include "GameplayState.h"
#include "CommandManager.h"
#include <functional>
#include <sstream>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <fstream>
#include "Room.h"
#include "Inventory.h"
#include "CommandManager.h"
#include "Item.h"

using namespace std;

GameplayState::GameplayState()
{
	
}

GameplayState::GameplayState(string fileName):playerInventory(1, "player")
{
	playerInventory.addItem(Item(0, "key for door", "Key"));
	int doorJump = 0;
	int ineNum = 0;
	int roomnum = 0;
	vector<string> lines;
	vector<string> roomPieces;
	vector<Door> Doors;
	string line;
	ifstream myfile(fileName);
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			lines.push_back(line);
		}
		myfile.close();
	}

	for (int i = lines.size() - 1; i >= 0; i--) {
		roomPieces = split(lines[i], ';');
		doorJump = 0;
		roomnum = 0;

		for (int a = 0; a < atoi(roomPieces[1].c_str()); a++)
		{
			Doors.push_back(Door(roomPieces[2 + doorJump], atoi(roomPieces[3 + doorJump].c_str()), roomPieces[4 + doorJump]));
			doorJump = doorJump + 3;
		}

		rooms.push_back(Room(roomPieces[0], Doors));
		int itemJUmp = doorJump;
		if (roomPieces.size() - 1 >= 2 + doorJump)
		{
			for (int k = 0; k < atoi(roomPieces[2 + doorJump].c_str()); k++)
			{
				rooms[roomnum].Inven.addItem(Item(i, roomPieces[3 + itemJUmp], roomPieces[4 + itemJUmp]));
				if (roomPieces[5 + itemJUmp] == "i")
				{
					inev.push_back(Inventory(ineNum, roomPieces[3 + itemJUmp]));
					inev[ineNum].addItem(Item(0, roomPieces[7 + itemJUmp], roomPieces[6 + itemJUmp]));
					ineNum++;
					itemJUmp = itemJUmp + 5;
				}
				else
					itemJUmp = itemJUmp + 2;
			}
		}
		//rooms[i].Inven.addItem(Item(i, roomPieces[3 + doorJump], roomPieces[4 + doorJump]));
		Doors.clear();
		roomnum++;
	}
	//inev.push_back(Inventory(0,"dsds"));
	//inev[0].addItem(Item(5 + doorJump, "xdsfdsf", "ned"));
	currentRoom = 0;



	//for (int i = lines.size() - 1; i >= 0; i--) {
		//cout << lines[i] << endl;
	//}
}

void GameplayState::display()
{
	rooms[currentRoom].display();
}

string GameplayState::getInput()
{
	string input;
	getline(cin, input);
	return input;
}

void GameplayState::update(string input)
{
	CommandManager comMan = CommandManager();
	vector<string> comand;
	
	comand = split(input, ' ');

	if(comand.size() > 0)
		comMan.runCommand(input, comand[0], *this);

}

vector<string> GameplayState::split(string str, char delimiter) {
	vector<string> internal;
	stringstream ss(str); // Turn the string into a stream.
	string tok;

	while (getline(ss, tok, delimiter)) {
		internal.push_back(tok);
	}

	return internal;
}

