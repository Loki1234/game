#pragma once

#include <iostream>
#include <string>
#include "GameplayState.h"

#ifndef COMMAND_H
#define COMMAND_H

using namespace std;

class Command
{
public:
	virtual void run(string input, GameplayState& world);
};
#endif 
