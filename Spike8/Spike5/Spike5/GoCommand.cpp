#include "Command .h"
#include "GoCommand.h"
#include <iostream>
#include <string>

using namespace std;

void GoCommand::run(string input, GameplayState& world) 
{
	int newloc;
	vector<string> comand;
	comand = world.split(input, ' ');

	if (comand.size() > 1)
	{
		for (int k = 0; k < world.rooms[world.currentRoom].doors.size(); k++)
		{
			if (world.rooms[world.currentRoom].doors[k].desc == comand[1])
			{
				if (!world.rooms[world.currentRoom].doors[k].locked)
				{
					newloc = world.rooms[world.currentRoom].findDoorLoc(comand[1]);
					if (newloc != 1000)
						world.currentRoom = newloc;
				}
				else 
					cout << "Door is Locked or Shut" << endl;
			}
		}				
	}
}
