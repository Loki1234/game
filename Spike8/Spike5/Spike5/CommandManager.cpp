#include "CommandManager.h"
#include <iostream>
#include <string>
#include "LookCommand.h"
#include "OpenCommand.h"
using namespace std;


CommandManager::CommandManager()
{
	
}

void CommandManager::runCommand(string rawInput,string firstInput, GameplayState& world)
{
	LookCommand look;
	OpenCommand open;
	//case statment here for each command 
	if(firstInput == "Go" || firstInput == "go" || firstInput == "gO")
		Go.run(rawInput, world); 
	if (firstInput == "Look" )
		look.run(rawInput, world); 
	if (firstInput == "Open")
		open.run(rawInput, world);
}