#pragma once
#include <iostream>
#include <string>

using namespace std;

#ifndef ITEM_H
#define ITEM_H

class Item
{

protected:
	int id;
	//string name;
	//string desc;

public:
	string desc;
	string name;
	Item();
	Item(int newid, string ItName, string descp);

	string display();

};
#endif 