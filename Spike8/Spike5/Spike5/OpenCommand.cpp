#include "Command .h"
#include "OpenCommand.h"
#include <iostream>
#include <string>

using namespace std;

void OpenCommand::run(string input, GameplayState& world)
{
	vector<string> comand;
	comand = world.split(input, ' ');
	bool dooropen = false;

	if (comand.size() == 2)
	{
		for (int k = 0; k < world.rooms[world.currentRoom].doors.size(); k++)
		{
			if (world.rooms[world.currentRoom].doors[k].desc == comand[1])
			{
				if (world.rooms[world.currentRoom].doors[k].locked)
				{
					cout << "Door is Locked requires key" << endl;
				}
				else
					cout << "Door is open" << endl;
			}			
		}
	}
	else if (comand.size() == 4)
	{
		for (int k = 0; k < world.rooms[world.currentRoom].doors.size(); k++)
		{
			if (world.rooms[world.currentRoom].doors[k].desc == comand[1])
			{
				for (int j = 0; j < world.playerInventory.Items.size(); j++)
				{
					if (world.playerInventory.Items[j].desc == comand[3])
					{
						if (world.rooms[world.currentRoom].doors[k].locked)
						{
							world.rooms[world.currentRoom].doors[k].unlockDoor();
							cout << world.rooms[world.currentRoom].doors[k].desc + " is unlocked" << endl;
							dooropen = true;
						}
					}
				}
			}
		}

		if (!dooropen)
			cout << "That did not work" << endl;
	}

}
